package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name, @Nullable final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description == null ? "" : description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return models.stream()
                .filter(m -> m.getProjectId() != null)
                .filter(m -> m.getProjectId().equals(projectId))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
