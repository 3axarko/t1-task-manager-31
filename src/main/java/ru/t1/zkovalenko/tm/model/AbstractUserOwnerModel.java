package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    private String userId;

}
