package ru.t1.zkovalenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Project complete by id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
