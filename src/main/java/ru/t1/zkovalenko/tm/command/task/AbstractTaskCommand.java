package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.exception.entity.TaskNotFoundException;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showTask(@Nullable final Task Task) {
        if (Task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + Task.getId());
        System.out.println("NAME: " + Task.getName());
        System.out.println("DESCRIPTION: " + Task.getDescription());
        System.out.println("STATUS: " + Status.toDisplayName(Task.getStatus()));
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task + "[" + task.getId() + "]" + "\tBinded to: " + task.getProjectId());
            index++;
        }
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
