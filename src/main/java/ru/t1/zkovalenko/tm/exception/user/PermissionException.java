package ru.t1.zkovalenko.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Permission is incorrect");
    }

}
