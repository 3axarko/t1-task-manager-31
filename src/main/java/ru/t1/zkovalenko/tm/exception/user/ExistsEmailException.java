package ru.t1.zkovalenko.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Email not exists");
    }

}
