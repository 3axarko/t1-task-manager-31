package ru.t1.zkovalenko.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.ServerAboutRequest;
import ru.t1.zkovalenko.tm.dto.request.ServerVersionRequest;
import ru.t1.zkovalenko.tm.dto.response.ServerAboutResponse;
import ru.t1.zkovalenko.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
