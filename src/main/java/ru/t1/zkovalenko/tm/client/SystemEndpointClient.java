package ru.t1.zkovalenko.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.ISystemEndpoint;
import ru.t1.zkovalenko.tm.dto.request.ServerAboutRequest;
import ru.t1.zkovalenko.tm.dto.request.ServerVersionRequest;
import ru.t1.zkovalenko.tm.dto.response.ServerAboutResponse;
import ru.t1.zkovalenko.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

}
